package be.kdg.java2.loggingdemo;


import be.kdg.java2.loggingdemo.domain.Dice;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main {
    private static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        URL configURL = Main.class.getResource("/logging.properties");
        if (configURL != null) {
            try (InputStream is = configURL.openStream()) {
                LogManager.getLogManager().readConfiguration(is);
            } catch (IOException e) {
                System.err.println("Configuratiebestand is corrupt");
            }
        } else {
            System.err.println("Configuratiebestand NIET GEVONDEN");
        }
        logger.info("Starting application...");
        new Dice().throwIt();
    }
}
