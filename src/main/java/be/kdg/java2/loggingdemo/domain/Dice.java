package be.kdg.java2.loggingdemo.domain;

import java.util.Random;
import java.util.logging.Logger;

public class Dice {
    private Logger logger = Logger.getLogger(this.getClass().getName());
    private int value;

    public void throwIt() {
        logger.severe("er is geworpen!");
        this.value = new Random().nextInt(1,7);
    }

    public int getValue() {
        return value;
    }
}
